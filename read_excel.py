import openpyxl


workbook = openpyxl.load_workbook('Emp_detailss.xlsx')


worksheet = workbook['sh']


for row in worksheet.iter_rows(min_row=2, values_only=True):
    Name, Designation, Salary_PM = row
    print(f"Name: {Name}, Designation: {Designation}, Salary: {Salary_PM}")
