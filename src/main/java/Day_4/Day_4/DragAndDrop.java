package Day_4.Day_4;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DragAndDrop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\vkeshavasa001\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
	     
	    driver.get("https://the-internet.herokuapp.com/");
	    driver.manage().window().maximize();
	    
	    driver.findElement(By.xpath("//a[contains(text(),'Drag')]")).click();
	    
	    WebElement a = driver.findElement(By.xpath("//div[@id='column-a']"));
	    WebElement b = driver.findElement(By.xpath("//div[@id='column-b']"));
	    
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	   
	    
	    Actions action = new Actions(driver);
	    
	    action.clickAndHold(a).moveToElement(b).release().build().perform();
	    
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    
	    //action.clickAndHold(b).moveToElement(a).release().build().perform();

	}

}
