package Day_4.Day_4;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class alert3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\vkeshavasa001\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
	     
	    driver.get("https://the-internet.herokuapp.com/");
	    driver.manage().window().maximize();
	    
	    driver.findElement(By.xpath("//a[text()='JavaScript Alerts']")).click();
	    
	    driver.findElement(By.xpath("//button[text()='Click for JS Prompt']")).click();
	    
	    Alert al = driver.switchTo().alert();
	    
	    System.out.println(al.getText());
	    
	    
	    al.sendKeys("Keshava");
	    
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    
	    al.accept();
	    
	   // alert.dismiss();

	}

}
